/// <reference types="cypress" />

context("customElement - broken", () => {
    it("customElement", () => {
        // when
        cy.visit("https://benjaminknauer.gitlab.io/cypress-custom-elements-bug");

        // then
        cy.get(".nonExistentElement").should("be.visible");
    });
});