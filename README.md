# Cypress Custom Elements Bug Demo

## Screenshots

Nomal Page Visit:  
![Nomal Page Visit](https://gitlab.com/benjaminknauer/cypress-custom-elements-bug/-/raw/master/screenshots/console-without-cypress.png "Nomal Page Visit")

Assertion Page Visit in Cypress:  
![Assertion Page Visit in Cypress](https://gitlab.com/benjaminknauer/cypress-custom-elements-bug/-/raw/master/screenshots/console-cypress.png "Assertion Page Visit in Cypress")