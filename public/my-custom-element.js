class MyCustomElement extends HTMLElement {
    constructor() {
        super();
        console.log("Constructor called");
    }

    static get observedAttributes() {
        return ["test"];
      }

    connectedCallback() {
        console.log("connectedCallback called");
    }

    disconnectedCallback() {
        console.log("disconnectedCallback called");
    }

    attributeChangedCallback(name, oldValue, newValue) {
        console.log(`attributeChangedCallback called - | name: ${name} | oldValue: ${oldValue} | newValue: ${newValue} |`);
    }

    adoptedCallback() {
        console.log("adoptedCallback called");
    }
}

window.customElements.define('my-custom-element', MyCustomElement);